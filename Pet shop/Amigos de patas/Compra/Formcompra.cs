﻿using Amigos_de_patas.RH.cadastros.classes.funcionario;
using Amigos_de_patas.RH.cadastros.classes.Produto;
using Amigos_de_patas.telas;
using Amigos_de_patas.telas.cadastros.classes.cadastrar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.cliente._3__modulo
{
    public partial class Formcompra : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();

        public Formcompra()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }
        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = lista;

            FuncionarioBusiness bs = new FuncionarioBusiness();
            List<Fruncionario_view> listar = bs.Listar();

            cboFuncionario.ValueMember = nameof(Fruncionario_view.ID);
            cboFuncionario.DisplayMember = nameof(Fruncionario_view.Nome);
            cboFuncionario.DataSource = listar;

        }
        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }




        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

            int qtd = Convert.ToInt32(txtQuantidade.Text);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            compra
        }
    }
}
