﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.DB.Compra
{
    class CompraDatabase
    {
        public int Salvar(CompraDTO dto)
        {

            string script = @"INSERT INTO tb_compra(
                            id_funcionario,
                            id_produto,
                            dt_compra)
                            VALUES(
                        
                            @id_funcionario,
                            @id_produto,
                            @dt_compra)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id_Funcionario));
            parms.Add(new MySqlParameter("id_produto", dto.Id_Produto));
            parms.Add(new MySqlParameter("dt_compra", dto.Data));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);



        }

        public void Alterar(CompraDTO dto)
        {
            string script = @"UDATE tb_compra
                            id_compra,
                            id_funcionario,
                            id_produto,
                            dt_compra

                    SET     @id_compra,
                            @id_funcionario,
                            @id_produto,
                            @dt_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_compra", dto.Id));
            parms.Add(new MySqlParameter("id_funcionario", dto.Id_Funcionario));
            parms.Add(new MySqlParameter("id_produto", dto.Id_Produto));
            parms.Add(new MySqlParameter("dt_compra", dto.Data));

            Database db = new Database();
             db.ExecuteInsertScript(script, parms);
        }

    }
}
