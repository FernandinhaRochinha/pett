﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.funcionario
{
    public class Fruncionario_view
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public string Departamento { get; set; }

        public int Idade { get; set; }

        public string CPF { get; set; }

        public string RG { get; set; }

        public decimal Salario { get; set; }

        public string Senha { get; set; }

        public string CEP { get; set; }
    }
}
