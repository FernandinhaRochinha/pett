﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Vendas
{
    class VendasDTO
    { 
        public int Id { get; set; }

        public string Pedido { get; set; }

        public int Quantidade { get; set; }

        public int Total { get; set;}


    }
}
