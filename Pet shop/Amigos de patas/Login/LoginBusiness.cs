﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Login
{
    class LoginBusiness
    {
        LoginDatabase db = new LoginDatabase();

        public bool VerificarRegistro (LoginDTO dto)
        {
            if (dto.Usuario == string.Empty)
            {
                throw new ArgumentException("O campo usuario deve ser preenchido !");
            }
            if (dto.Senha == string.Empty)
            {
                throw new ArgumentException("O campo senha é obrigatorio !");
            }

            
            bool logou = db.VerificarRegistro(dto);
            return logou;
        }
    }
}
