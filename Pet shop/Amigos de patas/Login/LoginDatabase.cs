﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.Login
{
    class LoginDatabase
    {
        public bool VerificarRegistro (LoginDTO dto)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE nm_funcionario = @nm_funcionario
                            AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }


        }
    }
}
