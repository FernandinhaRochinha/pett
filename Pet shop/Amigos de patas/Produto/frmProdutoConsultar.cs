﻿using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.RH.cadastros.classes.Produto
{
    public partial class frmProdutoConsultar : Form
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();

            ProdutoBusiness busines = new ProdutoBusiness();
            List<ProdutoDTO> listar = busines.Listar();

            dgvbuscar.AutoGenerateColumns = false;
            dgvbuscar.DataSource = listar;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = new ProdutoDTO();
            dto.Nome = txtnome.Text;

            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> consultar = business.Consultar(dto);

            dgvbuscar.AutoGenerateColumns = false;
            dgvbuscar.DataSource = consultar;


        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dgvfuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void dgvbuscar_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7 )
            {
                ProdutoDTO produto = dgvbuscar.Rows[e.RowIndex].DataBoundItem as ProdutoDTO;

                DialogResult r = MessageBox.Show($"Deseja realmente excluir esse Produto ?", "Amigos de patas",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProdutoBusiness business = new ProdutoBusiness();
                    business.Remover(produto.ID);

                    MessageBox.Show("Produto Removido com exito", "Amigos de Patas"
                                    , MessageBoxButtons.OK, MessageBoxIcon.Information);

                    ProdutoBusiness busines = new ProdutoBusiness();
                    List<ProdutoDTO> listar = busines.Listar();

                    dgvbuscar.AutoGenerateColumns = false;
                    dgvbuscar.DataSource = listar;
                }

            }
        }
    }
}
