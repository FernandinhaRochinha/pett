﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.Folha_de_pagamento.View
{
    class FolhaViewDTO
    {
        public int Id { get; set; }

        public decimal Horas_Trabalhadas { get; set; }

        public decimal Valor_Horas_Extras { get; set; }

        public decimal FGTS { get; set; }

        public decimal INSS { get; set; }

        public decimal IR { get; set; }

        public decimal Dependente { get; set; }

        public decimal Bruto { get; set; }

        public decimal Liquido { get; set; }

        public int Id_Funcionario { get; set; }

        public string Nome_Funcionario { get; set; }

        public decimal Salario_Funcionario { get; set; }
    }
}
