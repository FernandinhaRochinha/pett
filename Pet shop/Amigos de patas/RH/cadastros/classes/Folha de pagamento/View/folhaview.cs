﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.Folha_de_pagamento.View
{
    class FolhaView
    {
        public FolhaViewDTO Folha_Pagamento(FolhaViewDTO dto)
        {
            string script =
                @"SELECT * FROM view_folha_pagamento WHERE nm_funcionario = @nm_funcionario";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            FolhaViewDTO deteo = new FolhaViewDTO();
            while (reader.Read())
            {
                FolhaViewDTO dt = new FolhaViewDTO();
                dt.Id = reader.GetInt32("id_folha_pagamento");

                dt.Id_Funcionario = reader.GetInt32("id_funcionario");

                dt.Horas_Trabalhadas = reader.GetDecimal("nr_horas_trabalhadas");
                dt.Valor_Horas_Extras = reader.GetDecimal("vl_horas_extras");
                dt.FGTS = reader.GetDecimal("vl_FGTS");
                dt.INSS = reader.GetDecimal("vl_INSS");
                dt.IR = reader.GetDecimal("vl_IR");
                dt.Dependente = reader.GetDecimal("vl_dependente");
                dt.Bruto = reader.GetDecimal("vl_bruto");
                dt.Liquido = reader.GetDecimal("vl_liquido");

                dt.Nome_Funcionario = reader.GetString("nm_funcionario");

                dt.Salario_Funcionario = reader.GetDecimal("vl_salario");
                deteo = dt;
            }
            reader.Close();
            return deteo;

        }
    }
}
