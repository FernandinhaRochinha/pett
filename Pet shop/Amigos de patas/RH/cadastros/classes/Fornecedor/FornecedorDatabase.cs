﻿using _3_Telas.BásicosCarne;
using Amigos_de_patas.RH.cadastros.classes.funcionario.Fornecedor;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.RH.cadastros.classes.Fornecedor
{
    public class FornecedorDatabase
    {

        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_fornecedor (nm_fornecedor , 
                                                         dt_entrega ,       
                                                         ds_telefone , 
                                                         dt_cobranca , 
                                                         ds_rua,
                                                         ds_bairro,
                                                         ds_cep ,
                                                         ds_email ,
                                                         ds_uf ,
                                                         ds_atividade) 
                                    VALUES (@nm_fornecedor ,@dt_entrega ,@ds_telefone , @dt_cobranca , @ds_rua,@ds_bairro,@ds_cep ,@ds_email ,@ds_uf ,@ds_atividade)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Nome));
            parms.Add(new MySqlParameter("dt_entrega", dto.Entrega));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("dt_cobranca", dto.Cobranca));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_uf", dto.UF));
            parms.Add(new MySqlParameter("ds_atividade", dto.Atividade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FornecedorDTO> Listar()
        {
            string script = "select * from tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();

            while (reader.Read())

            {
                FornecedorDTO dtos = new FornecedorDTO();
                dtos.ID = reader.GetInt32("id_fornecedor");
                dtos.Nome = reader.GetString("nm_fornecedor");
                dtos.Rua = reader.GetString("ds_rua");
                dtos.Bairro = reader.GetString("ds_bairro");
                dtos.Cep = reader.GetString("ds_cep");
                dtos.Email = reader.GetString("ds_email");
                dtos.Telefone = reader.GetString("ds_telefone");               
                dtos.Cobranca = reader.GetDateTime("dt_cobranca");
                dtos.Entrega = reader.GetDateTime("dt_entrega");
                dtos.UF = reader.GetString("ds_cep");

                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }

        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FornecedorDTO> Consultar(FornecedorDTO dto)
        {
            string script =
                @"SELECT * FROM tb_fornecedor 
                           WHERE nm_fornecedor like @nm_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", "%" + dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();

            while (reader.Read())

            {
                FornecedorDTO dtos = new FornecedorDTO();
                dtos.ID = reader.GetInt32("id_fornecedor");
                dtos.Nome = reader.GetString("nm_fornecedor");
                dtos.Rua = reader.GetString("ds_rua");
                dtos.Bairro = reader.GetString("ds_bairro");
                dtos.Cep = reader.GetString("ds_cep");
                dtos.Email = reader.GetString("ds_email");
                dtos.Telefone = reader.GetString("ds_telefone");
                dtos.Cobranca = reader.GetDateTime("dt_cobranca");
                dtos.Entrega = reader.GetDateTime("dt_entrega");
                dtos.UF = reader.GetString("ds_uf");

                lista.Add(dtos);
            }
            reader.Close();
            return lista;

        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor set   id-fornecedor = @id_fornecedor,
                                                         nm_fornecedor = @nm_fornecedor, 
                                                         dt_entrega   = @dt_entrega ,       
                                                         ds_telefone  = @ds_telefone, 
                                                         dt_cobranca  = @dt_cobranca, 
                                                         ds_rua       = @ds_rua,
                                                         ds_bairro    = @ds_bairro,
                                                         ds_cep       = @ds_cep,
                                                         ds_email     = @ds_email,
                                                         ds_uf        = @ds_uf,
                                                         ds_atividade = @ds_atividade
                                                 WHERE id_funcionario = @id_funcionario"; 

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.ID));
            parms.Add(new MySqlParameter("nm_fornecedor", dto.Nome));
            parms.Add(new MySqlParameter("dt_entrega", dto.Entrega));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("dt_cobranca", dto.Cobranca));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_uf", dto.UF));
            parms.Add(new MySqlParameter("ds_atividade", dto.Atividade));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
    }

}
