﻿using Amigos_de_patas.RH.cadastros.classes.funcionario.Fornecedor;
using Amigos_de_patas.telas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.RH.cadastros.classes.Fornecedor
{
    public partial class frmFornecedorCadastrar : Form
    {
        public frmFornecedorCadastrar()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            FornecedorDTO dto = new FornecedorDTO();
            dto.Nome = txtNome.Text.Trim();
            dto.Rua = txtRua.Text.Trim();
            dto.Telefone = txtTelefone.Text.Trim();
            dto.Email = txtEmail.Text.Trim();
            dto.Entrega = dtpEntrega.MaxDate;
            dto.Bairro = txtBairro.Text.Trim();
            dto.Cep = txtCEP.Text.Trim();
            dto.Atividade = txtAtividade.Text.Trim();
            dto.UF = cboUF.SelectedItem.ToString();

            FornecedorBusiness bs = new FornecedorBusiness();
            bs.Salvar(dto);

            MessageBox.Show("Fornecedor cadastrado com sucesso", "Amigos de Patas",
                             MessageBoxButtons.OK, MessageBoxIcon.Information);

            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }
    }
}
