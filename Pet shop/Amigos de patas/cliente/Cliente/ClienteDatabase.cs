﻿using _3_Telas.BásicosCarne;
using Amigos_de_patas.RH.cadastros.classes.funcionario.Fornecedor;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.cliente.Cliente
{
    public class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_cliente , nm_sobrenome , ds_email , ds_rg , nm_animal , ds_raca , ds_telefone) 
                                    VALUES (@nm_cliente , @nm_sobrenome , @ds_email , @ds_rg , @nm_animal , @ds_raca , @ds_telefone)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("nm_sobrenome", dto.Sobrenome));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("nm_animal", dto.Animal));
            parms.Add(new MySqlParameter("ds_raca", dto.Raca));
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ClienteDTO> Consultar(ClienteDTO dto)
        {
            string script =
                @" SELECT * FROM tb_cliente 
                           WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();

            while (reader.Read())

            {
                ClienteDTO dtos = new ClienteDTO();
                dtos.ID = reader.GetInt32("id_cliente");
                dtos.Nome = reader.GetString("nm_cliente");              
                dtos.Email = reader.GetString("ds_email");
                dtos.Telefone = reader.GetString("ds_telefone");
                dtos.RG = reader.GetString("ds_rg");
                dtos.Sobrenome = reader.GetString("nm_sobrenome");


                lista.Add(dtos);
            }
            reader.Close();
            return lista;

        }
        public void Remover(int ID)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<ClienteDTO> Listar()
        {
            string script = "select * from tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();

            while (reader.Read())

            {
                ClienteDTO dtos = new ClienteDTO();
                dtos.ID = reader.GetInt32("id_cliente");
                dtos.Nome = reader.GetString("nm_cliente");
                dtos.Email = reader.GetString("ds_email");
                dtos.Telefone = reader.GetString("ds_telefone");
                dtos.RG = reader.GetString("ds_rg");
                dtos.Sobrenome = reader.GetString("nm_sobrenome");



                lista.Add(dtos);
            }
            reader.Close();
            return lista;
        }


    }
}
