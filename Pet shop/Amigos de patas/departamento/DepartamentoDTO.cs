﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.telas.cadastros.classes.cadastro_de_departamento
{
    public class DepartamentoDTO
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public string Descricao { get; set; }
    }
}
