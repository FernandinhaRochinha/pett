﻿using Amigos_de_patas.cliente;
using Amigos_de_patas.cliente._3__modulo;
using Amigos_de_patas.cliente.Cliente;
using Amigos_de_patas.RH.cadastros.classes.Fornecedor;
using Amigos_de_patas.RH.cadastros.classes.Produto;
using Amigos_de_patas.telas.cadastros;
using Amigos_de_patas.telas.Consultas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.telas
{
    public partial class frminicio : Form
    {
        public frminicio()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void produtoToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFuncionario tela = new frmFuncionario();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmfuncionarios tela = new frmfuncionarios();
            tela.Show();
            this.Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void transporteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmFuncionario tela = new frmFuncionario();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmfuncionarios tela = new frmfuncionarios();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmdepartamento tela = new frmdepartamento();
            tela.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmFornecedorCadastrar tela = new frmFornecedorCadastrar();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmFornecedorConsultar tela = new FrmFornecedorConsultar();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmProdutoCadastrar tela = new frmProdutoCadastrar();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmProdutoConsultar tela = new frmProdutoConsultar();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmcadastro tela = new frmcadastro();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmConsultar tela = new frmConsultar();
            tela.Show();
            this.Hide();
        }

        private void novaCompraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formcompra tela = new Formcompra();
            tela.Show();
            this.Hide();
        }
    }
}
