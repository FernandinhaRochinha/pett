﻿using Amigos_de_patas.telas.cadastros.classes.cadastrar;
using Amigos_de_patas.telas.cadastros.classes.cadastro_de_departamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.telas.cadastros
{
    public partial class frmFuncionario : Form
    {
        public frmFuncionario()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            DepartamentoBusiness business = new DepartamentoBusiness();
            List<DepartamentoDTO> lista = business.Listar();

            cbodepartamento.ValueMember = nameof(DepartamentoDTO.ID);
            cbodepartamento.DisplayMember = nameof(DepartamentoDTO.Nome);
            cbodepartamento.DataSource = lista;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DepartamentoDTO departamento = cbodepartamento.SelectedItem as DepartamentoDTO;           

                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = txtnome.Text.Trim();
                dto.Departamento = departamento.ID;
                dto.Idade = Convert.ToInt32(txtidade.Text.Trim());
                dto.RG = txtrg.Text.Trim();
                dto.CPF = txtcpf.Text.Trim();
                dto.Salario = Convert.ToDecimal(txtsalario.Text.Trim());
                dto.Senha = txtsenha.Text.Trim();
                dto.CEP = txtcep.Text.Trim();

                FuncionarioBusiness businesse = new FuncionarioBusiness();
                businesse.Salvar(dto);

            MessageBox.Show("Funcionario cadastrado com sucesso", "Amigos de Patas",
                             MessageBoxButtons.OK, MessageBoxIcon.Information);

            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void frmFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();

        }

        private void cbodepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

