﻿using Amigos_de_patas.RH;
using Amigos_de_patas.RH.cadastros.classes.funcionario;
using Amigos_de_patas.telas.cadastros.classes.cadastrar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amigos_de_patas.telas.Consultas
{
    public partial class frmfuncionarios : Form
    {
        public frmfuncionarios()
        {
            InitializeComponent();

            FuncionarioBusiness busines = new FuncionarioBusiness();
            List<Fruncionario_view> listar = busines.Listar();

            dgvfuncionario.AutoGenerateColumns = false;
            dgvfuncionario.DataSource = listar;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Fruncionario_view dto = new Fruncionario_view();
             dto.Nome = txtnome.Text.Trim();
             FuncionarioBusiness business = new FuncionarioBusiness();
            List<Fruncionario_view> consultar = business.Consultar(dto);

            dgvfuncionario.AutoGenerateColumns = false;
            dgvfuncionario.DataSource = consultar;
        }
            catch (ArgumentException)
            {
            MessageBox.Show("Preencha todos os campos.");
            }
            catch (Exception)
            {

                MessageBox.Show("Oh Oh! Algo de errado!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frminicio tela = new frminicio();
            tela.Show();
            this.Close();
        }

        private void dgvfuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 9)
            {
                Fruncionario_view f = dgvfuncionario.Rows[e.RowIndex].DataBoundItem as Fruncionario_view;

                frmAlterar tela = new frmAlterar();
                tela.LoadScreen(f);
                tela.ShowDialog();

                this.Hide();

            }

            if (e.ColumnIndex == 8)
            {
                Fruncionario_view funcionario = dgvfuncionario.Rows[e.RowIndex].DataBoundItem as Fruncionario_view;

                DialogResult r = MessageBox.Show($"Deseja realmente excluir esse Funcionario ?", "Amigos de patas",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(funcionario.ID);

                    MessageBox.Show("Funcionario Removido com exito", "Amigos de Patas"
                                    , MessageBoxButtons.OK , MessageBoxIcon.Information);

                    FuncionarioBusiness busines = new FuncionarioBusiness();
                    List<Fruncionario_view> listar = busines.Listar();

                    dgvfuncionario.AutoGenerateColumns = false;
                    dgvfuncionario.DataSource = listar;
                }
            }
        }

        private void frmfuncionarios_Load(object sender, EventArgs e)
        {

        }
    }
}
