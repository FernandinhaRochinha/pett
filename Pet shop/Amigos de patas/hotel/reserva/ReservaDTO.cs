﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amigos_de_patas.hotel.hotel
{
    class ReservaDTO
    {
        public int ID { get; set; }

        public int Quarto { get; set; }

        public int Cliente { get; set; }

        public DateTime Hospedagem { get; set; }

        public string especificacao { get; set; }
    }
}
